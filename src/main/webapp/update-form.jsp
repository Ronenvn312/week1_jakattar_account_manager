<%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 9/22/2023
  Time: 11:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Update Account</h1>
<form action="update" method="put" class="styled-div">
    <p>id: </p> <input value="<c:out value='${account.accountId}'/>" type="text" name="account_id"  /> <br>
    <p>username: </p> <input value="<c:out value='${account.fullName}'/>" type="text" name="full_name" /> <br>
    <p>password: </p> <input value="<c:out value='${account.password}'/>" type="password" name="psw" /> <br>
    <p>email: </p> <input value="<c:out value='${account.email}'/>"  type="text" name="email"/> <br>
    <p>phone: </p> <input value="<c:out value='${account.phone}'/>" type="text" name="phone" /> <br>
    <input type="submit" value="update" />
    <input type="reset" value="Reset" />
</form>
</body>
</html>
