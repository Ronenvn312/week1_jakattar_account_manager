package com.example.demo1.dao;

import com.example.demo1.data.entity.Account;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ConnectionDB {

    private static String SELECT_ACCOUNT = "select * from account where status = 1";
    private static String UPDATE_ACCOUNT = "update account set full_name= ?, password= ?, email = ?, phone = ?, status = ? where account_id = ?,";
    private static String DELETE_ACCOUNT = "update account set status = -1  where account_id = ?";


    public ConnectionDB () {

    }

    protected Connection getConnection()
            throws SQLException, ClassNotFoundException
    {
        // Initialize all the information regarding
        // Database Connection
        String dbDriver = "com.mysql.jdbc.Driver";
        String dbURL = "jdbc:mysql://localhost:3306/mydb?useSSL=false";
        // Database name to access
        String dbUsername = "root";
        String dbPassword = "Example@2022#";

        Class.forName(dbDriver);
        Connection con = DriverManager.getConnection(dbURL,
                dbUsername,
                dbPassword);
        return con;
    }

    public List<Account> getList() {
        List<Account> accounts = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement(SELECT_ACCOUNT);
            ResultSet rs = preparedStatement.executeQuery();
//            out.println("<html><body>");
//            while (rs.next()) {
//                out.println("<p>" + rs.getString("account_id") + "</p>");
//                out.println("<p>" + rs.getString("full_name") + "</p>");
//                out.println("<p>" + rs.getString("password") + "</p>");
//                out.println("<p>" + rs.getString("email") + "</p>");
//                out.println("<p>" + rs.getString("phone") + "</p>");
//                out.println("<p>" + rs.getInt("status") + "</p>");
//                out.println("<input  type='button' value='delete'/>");
//                out.println("<p>" + rs.getInt("status") + "</p>");
//            }
//            out.println("</body></html>");
            while (rs.next()) {
                String accountId = rs.getString("account_id");
                String fullname = rs.getString("full_name");
                String password = rs.getString("password");
                String email = rs.getString("email");
                String phone = rs.getString("phone");
                int status = rs.getInt("status");
                accounts.add(new Account(accountId,fullname,password,email,phone,status));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return accounts;
    }
    public void insertAccount(Account account) {
        try {
            // Hello
            PreparedStatement st = getConnection()
                    .prepareStatement("insert into account values (?, ?, ?, ?, ?, 1 )");

            st.setString(1, account.getAccountId());
            st.setString(2, account.getFullName());
            st.setString(3, account.getPassword());
            st.setString(4, account.getEmail());
            st.setString(5, account.getPhone());
            st.executeUpdate();
            System.out.println(st);
            // Close all the connections
            st.close();
            getConnection().close();
        }   catch (Exception e) {
            e.printStackTrace();
        }
    }
    public boolean deleteAccount(String id) throws SQLException {
        boolean rowDeleted;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_ACCOUNT);) {
            statement.setString(1, id);
            rowDeleted = statement.executeUpdate() > 0;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return rowDeleted;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

    public void updateAccount(Account account) {
        try {
            // Hello
            PreparedStatement st = getConnection()
                    .prepareStatement(UPDATE_ACCOUNT);


            st.setString(1, account.getFullName());
            st.setString(2, account.getPassword());
            st.setString(3, account.getEmail());
            st.setString(4, account.getPhone());
            st.setString(5, account.getAccountId());
            st.executeUpdate();
            System.out.println(st);
            // Close all the connections
            st.close();
            getConnection().close();
        }   catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Account getAccount(String id) {
        Account account = null;
        // Step 1: Establishing a Connection
        try (Connection connection = getConnection();
             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement("select * from account where account_id = ? ");) {
            preparedStatement.setString(1, id);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                String accountId = rs.getString("account_id");
                String fullname = rs.getString("full_name");
                String password = rs.getString("password");
                String email = rs.getString("email");
                String phone = rs.getString("phone");
                int status = rs.getInt("status");
                account =  new Account(accountId,fullname,password,email,phone,status);
            }
        } catch (SQLException e) {
            printSQLException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return account;
    }
}
