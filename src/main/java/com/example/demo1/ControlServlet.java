package com.example.demo1;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.example.demo1.dao.ConnectionDB;
import com.example.demo1.data.entity.Account;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "ControlServlet", value = "/")
public class ControlServlet extends HttpServlet {
    private String message;
    private ConnectionDB connectionDB;


    @Override
    public void init() {
        message = "Hello word";
        connectionDB = new ConnectionDB();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();

        try {
            switch (action) {
                case "/new":
                    showNewForm(request, response);
                    break;
                case "/delete":
                    deleteAccount(request, response);
                    break;
                case "/update":
                    updateAccount(request, response);
                    break;
                case "/update-show":
                    showFormUpdate(request, response);
                    break;
                case "/insert":
                    insertAccount(request, response);
                    break;
                default:
                    listAccount(request, response);
                    break;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPut(req, resp);
        doGet(req, resp);
    }

    private void updateAccount(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String accountId = request.getParameter("account_id");
        String fullName = request.getParameter("full_name");
        String password = request.getParameter("psw");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        Account account = new Account(accountId, fullName, password, email, phone, 1);
        connectionDB.updateAccount(account);
        response.sendRedirect("list");
    }

    private void listAccount(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        List<Account> listAccount = connectionDB.getList();
        request.setAttribute("listAccount", listAccount);
        RequestDispatcher dispatcher = request.getRequestDispatcher("list-account.jsp");

        dispatcher.forward(request, response);
    }

    private void insertAccount(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        String accountId = request.getParameter("account_id");
        String fullName = request.getParameter("full_name");
        String password = request.getParameter("psw");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        Account account = new Account(accountId, fullName, password, email, phone, 1);
        connectionDB.insertAccount(account);
        response.sendRedirect("list");
    }

    private void deleteAccount(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        String id = request.getParameter("id");
        connectionDB.deleteAccount(id);
        response.sendRedirect("list");

    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
        dispatcher.forward(request, response);
    }

    private void showFormUpdate(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        Account account = connectionDB.getAccount(id);
        request.setAttribute("account", account);
        RequestDispatcher dispatcher = request.getRequestDispatcher("update-form.jsp");
        dispatcher.forward(request, response);
    }

    public void destroy() {
    }
}